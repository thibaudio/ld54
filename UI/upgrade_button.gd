extends HoverButton

@export var next_levels: Array[GameStateResource]
var current_level = 0

var original_name

func _ready():
	original_name = text
	Events.upgrade_selected.connect(_on_upgrade_selected)

func _on_pressed():
	if not GameManager.current_game_state.can_apply(changes):
		return
	GameManager.current_game_state.apply(changes)
	GameManager.upgrade_selected()
	if len(next_levels) > current_level:
		changes = next_levels[current_level]
		current_level += 1
		text = original_name + " %d" % (current_level + 1)
	else:
		queue_free()

func _on_upgrade_selected(selected: bool):
	disabled = selected
