extends Button
class_name HoverButton

@export var changes: GameStateResource

func _on_mouse_entered():
	Events.over_upgrade.emit(self)


func _on_mouse_exited():
	Events.not_over_upgrade.emit(self)


func _on_focus_entered():
	Events.over_upgrade.emit(self)


func _on_focus_exited():
	Events.not_over_upgrade.emit(self)
