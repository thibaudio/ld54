extends HoverButton

func _ready():
	Events.game_state_changed.connect(_on_game_state_changed)
	_on_game_state_changed()
	
func _on_game_state_changed():
	changes = GameStateResource.new()
	changes.money = -GameManager.current_game_state.build_cost
	changes.day = GameManager.current_game_state.build_time
