extends Control

@onready var label = $HBoxContainer/Label
@onready var label_2 = $HBoxContainer/Label2
@onready var title = $HBoxContainer/Title

@export var displayed: Globals.GameStateType

var shaking = 0.
var original_position
var shake_duration = .5

func _ready():
	Events.over_upgrade.connect(_handle_over_upgrade)
	Events.not_over_upgrade.connect(_handle_not_over_upgrade)
	Events.game_state_changed.connect(init)
	Events.not_enought.connect(_handle_not_enought)
	original_position = position.x
	label_2.hide()
	init()

func init():
	match displayed:
		Globals.GameStateType.CurrentMoney:
			title.text = "Cash: "
			label.text = Utils.format_money(GameManager.current_game_state.money)
		Globals.GameStateType.LaunchCost:
			title.text = "Launch cost: "
			label.text = Utils.format_money(GameManager.current_game_state.launch_cost)
		Globals.GameStateType.Days:
			title.text = "Days: "
			label.text = str(GameManager.current_game_state.day)
		Globals.GameStateType.BuildTime:
			title.text = "Build time: "
			label.text = str(GameManager.current_game_state.build_time)
		Globals.GameStateType.BuildCost:
			title.text = "Build cost: "
			label.text = Utils.format_money(GameManager.current_game_state.build_cost)
		Globals.GameStateType.WeightCapacity:
			title.text = "Weight capacity: "
			label.text = Utils.format_weight(GameManager.current_game_state.weight_capacity)
		Globals.GameStateType.Risk:
			title.text = "Risks: "
			label.text = Utils.format_percent(GameManager.current_game_state.risk)
		Globals.GameStateType.PeopleCapacity:
			title.text = "Number of Passengers: "
			label.text = str(GameManager.current_game_state.people_capacity)
		Globals.GameStateType.TicketCost:
			title.text = "Ticket cost: "
			label.text = Utils.format_money(GameManager.current_game_state.ticket_cost)
		Globals.GameStateType.Reputation:
			title.text = "Reputation: "
			label.text = Utils.format_percent(GameManager.current_game_state.reputation)
		Globals.GameStateType.Research:
			title.text = "Research per cycles: "
			label.text = str(GameManager.current_game_state.researches_per_rounds)
			
func _handle_over_upgrade(upgrade: Button):
	if upgrade == null || upgrade.changes == null:
		return
		
	match displayed:
		Globals.GameStateType.CurrentMoney:
			format_money_upgrade(upgrade.changes.money)
		Globals.GameStateType.LaunchCost:
			format_money_upgrade(upgrade.changes.launch_cost)
		Globals.GameStateType.Days:
			format_flat_upgrade(upgrade.changes.day)
		Globals.GameStateType.BuildTime:
			format_flat_upgrade(upgrade.changes.build_time)
		Globals.GameStateType.BuildCost:
			format_money_upgrade(upgrade.changes.build_cost)
		Globals.GameStateType.WeightCapacity:
			format_weight_upgrade(upgrade.changes.weight_capacity)
		Globals.GameStateType.Risk:
			format_percent_upgrade(upgrade.changes.risk)
		Globals.GameStateType.PeopleCapacity:
			format_flat_upgrade(upgrade.changes.people_capacity)
		Globals.GameStateType.TicketCost:
			format_money_upgrade(upgrade.changes.ticket_cost)
		Globals.GameStateType.Reputation:
			format_percent_upgrade(upgrade.changes.reputation)
		Globals.GameStateType.Research:
			format_flat_upgrade(upgrade.changes.researches_per_rounds)

func format_percent_upgrade(value):
	if value == 0:
		return
	label_2.show()
	if value > 0:
		label_2.text = " + " + Utils.format_percent(value)
	else :
		label_2.text = " - " + Utils.format_percent(abs(value))
				
func format_weight_upgrade(value):
	if value == 0:
		return
	label_2.show()
	if value > 0:
		label_2.text = " + " + Utils.format_weight(value)
	else :
		label_2.text = " - " + Utils.format_weight(abs(value))
				
func format_flat_upgrade(value):
	if value == 0:
		return
	label_2.show()
	if value > 0:
		label_2.text = " + " + str(value)
	else :
		label_2.text = " - " + str(abs(value))
	
func format_money_upgrade(value):
	if value == 0:
		return
	label_2.show()
	if value > 0:
		label_2.text = " + " + Utils.format_money(value)
	else :
		label_2.text = " - " + Utils.format_money(abs(value))
	
func _handle_not_over_upgrade(upgrade: Button):
	label_2.hide()

func _handle_not_enought(what: Globals.GameStateType):
	if what == displayed:
		shake()

func shake():
	shaking = shake_duration

func _process(delta):
	if shaking <= 0:
		position.x = original_position
		return
	shaking -= delta
	position.x = Tween.interpolate_value(original_position, randf_range(-10,10), shaking, shake_duration, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
