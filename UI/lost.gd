extends CanvasLayer
@onready var color_rect = $ColorRect
@onready var label_2 = $Label2

func _on_new_game_button_pressed(): 
	GameManager.new_game()

func _on_quit_button_pressed():
	get_tree().quit()

func _on_main_menu_button_pressed():
	GameManager.main_menu()

func _ready():
	label_2.text = "You lasted %d days" % GameManager.current_game_state.day
