extends Node
class_name Utils


static func format_money(value):
	if value >= 10000:
		return "$%.3fB" % (float(value) / 1000.)
	return "$"+str(value)+"MM"

static func format_weight(value):
	return str(value) + "t"
	
static func format_percent(value):
	return "%03d" % (value * 100) + "%"
