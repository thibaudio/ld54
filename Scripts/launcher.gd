extends Sprite2D

var x_shaking_strenght = 40
var y_shaking_strenght = 5
var original_position
@export var is_shaking = false

func _ready():
	original_position = position

func shaking():
	is_shaking = true
	
func stop_shaking():
	is_shaking = false
	
func _process(delta):
	if not is_shaking:
		return
	
	var dest = original_position + Vector2(randf_range(-x_shaking_strenght, x_shaking_strenght), randf_range(-y_shaking_strenght, y_shaking_strenght))
	
	position = lerp(position, dest, delta )
