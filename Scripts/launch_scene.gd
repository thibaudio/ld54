extends Node2D

# Rocket
@onready var launcher = $Launcher
@onready var support = $Support
@onready var smoke_effect = $Launcher/Smoke/SmokeEffect
@onready var smoke_effect_2 = $Launcher/Smoke/SmokeEffect2
@onready var explosion_effect = $Launcher/Explosion/ExplosionEffect
@onready var explosion_effect_2 = $Launcher/Explosion/ExplosionEffect2
@onready var shuttle = $Launcher/Shuttle

# Global UI
@onready var global_ui = $GlobalUI

# Launch UI
@onready var launch_button = $LaunchTimer/LaunchButton
@onready var launch_timer_display = $LaunchTimer/LaunchTimerDisplay

# Build UI
@onready var build_ui = $BuildUI
@onready var build_button = $BuildUI/BuildButton

# FlightPrep UI
@onready var flight_prep_ui = $FlightPrepUI
@onready var filled_flight_button = $FlightPrepUI/FilledFlight

# Launched Scenes
@onready var launch_success = $launch_success


@export var starting_state = States.Ready
enum States { Ready, Launching, LaunchedFail, LaunchedSuccess, Build, ToBeFilled }
var current_state: States

var launching_in_seconds = 0.

var _support_original_position;
var _rocket_original_position;

var _support_tweener

# Called when the node enters the scene tree for the first time.
func _ready():
	current_state = starting_state
	launch_timer_display.hide()
	update_ui()
	_support_original_position = support.position
	_rocket_original_position = launcher.position

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	match current_state:
		States.Launching:
			_handle_launching(delta)
		States.ToBeFilled:
			_handle_to_be_filled(delta)
		

func _enter_launched_state():
	var success = randf_range(0., 1.)
	if success > GameManager.current_game_state.risk:
		current_state = States.LaunchedSuccess
		launch_success.show()
		launch_success.play_success()
	else:
		current_state = States.LaunchedFail
		launch_success.show()
		launch_success.play_fail()

	update_ui()
	
func _handle_to_be_filled(delta):
	if Input.is_action_just_pressed("mouse_click"):
		_support_tweener.stop()
		support.position.x = _support_original_position.x
		ready_launcher()
		
func _handle_launching(delta):
	if Input.is_action_just_pressed("mouse_click"):
		launcher.position.y = -500
		_support_tweener.stop()
		support.position.x = -50
		
		_on_launch_done()
		launching_in_seconds = -1
		
	if launching_in_seconds <= 0:
		launch_timer_display.hide()
		return
		
	launching_in_seconds = launching_in_seconds - delta
	
	if launching_in_seconds <= 0 :
		launch_timer_display.hide()
		var tween = create_tween()
		tween.tween_property(launcher, "position:y", -500, 2.0).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_EXPO)
		tween.finished.connect(_on_launch_done)
	else:
		launch_timer_display.text = "%.0f" % launching_in_seconds
		
	if launching_in_seconds <= 6 :
		smoke_effect.emitting = true
		smoke_effect_2.emitting = true

	if launching_in_seconds <= 3 :
		explosion_effect.emitting = true
		explosion_effect_2.emitting = true


func _on_launch_button_pressed():
	if launching_in_seconds > 0:
		return
	
	if not GameManager.current_game_state.can_apply(launch_button.changes):
		return
	
	current_state = States.Launching
	update_ui()
	GameManager.current_game_state.apply(launch_button.changes)
	launch_timer_display.show()
	launching_in_seconds = 10
	support.play("Closed")
	_support_tweener = create_tween()
	_support_tweener.tween_property(support, "position:x", -50, 4.0).set_ease(Tween.EASE_IN)

func update_ui():
	match current_state:
		States.Launching:
			global_ui.hide()
			launch_button.hide()
			build_ui.hide()
			flight_prep_ui.hide()
		States.Build:
			launch_button.hide()
			build_ui.show()
			global_ui.show()
			flight_prep_ui.hide()
		States.Ready:
			build_ui.hide()
			launch_button.show()
			global_ui.show()
			flight_prep_ui.hide()
		States.ToBeFilled:
			build_ui.hide()
			launch_button.hide()
			global_ui.show()
			flight_prep_ui.show()
			
func _on_launch_done():
	_enter_launched_state()

func ready_launcher():
	smoke_effect.emitting = false
	smoke_effect_2.emitting = false
	explosion_effect.emitting = false
	explosion_effect_2.emitting = false
	launcher.position = _rocket_original_position

	support.play("Open")
	shuttle.hide()

	update_ui()

func _on_build_button_pressed():
	if not GameManager.current_game_state.can_apply(build_button.changes):
		GameManager.lost_money()
	
	build_ui.hide()
	GameManager.current_game_state.apply(build_button.changes)
	current_state = States.ToBeFilled
	_support_tweener = create_tween()
	_support_tweener.tween_property(support, "position:x", _support_original_position.x, 4.0).set_ease(Tween.EASE_OUT)
	_support_tweener.finished.connect(ready_launcher, CONNECT_ONE_SHOT)


func _on_test_flight_pressed():
	current_state = States.Ready
	update_ui()


func _on_filled_flight_pressed():
	if not GameManager.current_game_state.can_apply(filled_flight_button.changes):
		return
		
	GameManager.current_game_state.apply(filled_flight_button.changes)
	current_state = States.Ready
	shuttle.show()
	update_ui()


func _on_launch_success_on_finish():
	if current_state == States.LaunchedFail:
		GameManager.lost()
		return
	Transitions.play_exit_transition()
	if GameManager.current_game_state.first_launch:
		GameManager.current_game_state.reputation = 0.5
		GameManager.current_game_state.first_launch = false
	else:
		GameManager.current_game_state.reputation += 0.1
	Events.game_state_changed.emit()
	await Transitions.transition_completed
	current_state = States.Build
	launch_success.hide()
	update_ui()
	GameManager.reset_upgrade()
	Transitions.play_enter_transition()

