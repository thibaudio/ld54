extends Resource
class_name GameStateResource

# Build rocket
@export var build_time: int
@export var build_cost: int

# Rocket params
@export var weight_capacity: int
@export var risk: float
@export var people_capacity: int
@export var launch_cost: int

# Operations
@export var money: int
@export var ticket_cost: int
@export var reputation: float
@export var day: int

@export var first_launch: bool
@export var researches_per_rounds: int

func can_apply(changes: GameStateResource) -> bool:
	if weight_capacity + changes.weight_capacity < 0:
		Events.not_enought.emit(Globals.GameStateType.WeightCapacity)
		return false
	if money + changes.money < 0:
		Events.not_enought.emit(Globals.GameStateType.CurrentMoney)
		return false
	if risk + changes.risk > 1:
		Events.not_enought.emit(Globals.GameStateType.Risk)
		return false
	if reputation + changes.reputation < 0:
		Events.not_enought.emit(Globals.GameStateType.Reputation)
		return false
	return true

func apply(changes: GameStateResource):
	if changes.build_time != 0:
		build_time += changes.build_time
	if changes.build_cost != 0:
		build_cost += changes.build_cost
		
	if changes.weight_capacity != 0:
		weight_capacity += changes.weight_capacity		
	if changes.risk != 0:
		risk = clamp(risk+changes.risk, 0., 1.)
	if changes.people_capacity != 0:
		people_capacity += changes.people_capacity		
		
	if changes.money != 0:
		money += changes.money
		
	if changes.ticket_cost != 0:
		ticket_cost += changes.ticket_cost		
	if changes.reputation != 0:
		reputation = clamp(reputation+changes.reputation, 0., 1.)
	if changes.day != 0:
		day += changes.day
	launch_cost += changes.launch_cost
	researches_per_rounds += changes.researches_per_rounds
	Events.game_state_changed.emit()
