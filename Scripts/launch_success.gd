extends Node2D

signal on_finish

@onready var animation_player = $AnimationPlayer

var playing = false
var anims_success = [ "success1", "success2", "success3"]
var anims_fails = ["fail1"]
var anims
var index = 0
func _unhandled_input(event):
	if not playing:
		return
	if event is InputEventMouseButton && event.is_action_pressed("mouse_click"):
		next()

func play_success():
	anims = anims_success
	play()
	
func play_fail():
	anims = anims_fails
	play()

func play():
	animation_player.play("RESET")
	playing = true
	index = -1
	next()

func next():
	if not playing:
		return
	index += 1
	if index == len(anims):
		playing = false
		animation_player.play("RESET")
		on_finish.emit()
		return
		
	animation_player.play(anims[index])

func _on_animation_player_animation_finished(anim_name):
	next()
