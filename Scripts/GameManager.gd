extends Node


var inital_game_state = preload("res://initial_game_state.tres") as GameStateResource
var current_game_state:
	get:
		if current_game_state == null:
			current_game_state = inital_game_state.duplicate()
		return current_game_state
	set(value):
		current_game_state = value

var launch_scene = preload("res://Scenes/launch_scene.tscn")
var title_scene = preload("res://Core/MainMenu/title_screen.tscn")
var lost_scene = preload("res://UI/lost.tscn")
var lost_money_scene = preload("res://UI/lost_money.tscn")

func new_game():
	current_game_state = inital_game_state.duplicate()
	reset_upgrade()
	go_to_launch()

func main_menu():
	Transitions.change_level(title_scene)
	#get_tree().change_scene_to_packed(title_scene)
	
func go_to_launch():
	Transitions.change_level(launch_scene)
	#get_tree().change_scene_to_packed(launch_scene)

func lost():
	get_tree().change_scene_to_packed(lost_scene)
	
func lost_money():
	get_tree().change_scene_to_packed(lost_money_scene)

func _ready():
	Events.game_state_changed.connect(_on_game_state_changed)
	
func _on_game_state_changed():
	current_game_state.ticket_cost = lerp(1, 60, current_game_state.reputation)

var available_upgrades
func reset_upgrade():
	available_upgrades = current_game_state.researches_per_rounds
	Events.upgrade_selected.emit(false)

func upgrade_selected():
	available_upgrades -= 1
	if available_upgrades == 0:
		Events.upgrade_selected.emit(true)
