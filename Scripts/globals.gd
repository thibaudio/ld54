class_name Globals

enum GameStateType {
	CurrentMoney, LaunchCost, Days, BuildTime, BuildCost, WeightCapacity, Risk, PeopleCapacity, TicketCost, Reputation, Research
}
