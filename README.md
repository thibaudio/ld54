# Space - Limited edition
Made during [Ludum Dare 54](https://ldjam.com/events/ludum-dare/54/space-limited-edition).  
Play it on itch.io: https://thibaudio.itch.io/space-limited-edition

Going to space is not for everyone!

Manage a space travel agency in this (very) light tycoon.

Thanks for playing - I hope you’ll enjoy it.