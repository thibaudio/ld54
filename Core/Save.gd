extends Node
class_name Save

static func write(data, file_path) -> void:
	var file := FileAccess.open(file_path, FileAccess.WRITE)
	if file == null:
		printerr("Could not open the file %s. Error code: %s" % [file_path, FileAccess.get_open_error()])
		return
	
	file.store_string(data)
	file.close()

static func read(file_path):
	if not FileAccess.file_exists(file_path):
		return null
		
	var file := FileAccess.open(file_path, FileAccess.READ)
	if file == null:
		printerr("Could not open the file %s. Error code: %s" % [file_path, FileAccess.get_open_error()])
		return null
		
	var content = file.get_as_text()
	return content
