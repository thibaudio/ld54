extends Node

@onready var audio_players: = $AudioPlayers
@onready var music_player = $MusicPlayer

func _ready():
	Events.options_changed.connect(_on_options_changed)

func play_sound(sound):
	for player in audio_players.get_children():
		if not player.playing:
			player.stream = sound
			player.play()
			break

func _on_master_volume_changed(volume):
	AudioServer.set_bus_volume_db(0, lerp(-80, 6, volume))
func _on_effects_volume_changed(volume):
	AudioServer.set_bus_volume_db(2, lerp(-80, 6, volume))
func _on_music_volume_changed(volume):
	AudioServer.set_bus_volume_db(1, lerp(-80, 6, volume))

func _on_options_changed():
	_on_master_volume_changed(OptionsController.options.MasterVolume)
	_on_effects_volume_changed(OptionsController.options.EffectsVolume)
	_on_music_volume_changed(OptionsController.options.MusicVolume)
