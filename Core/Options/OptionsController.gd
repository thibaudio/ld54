extends Node

const OPTIONS_PATH := "user://options.json"

var options: Resource = Options.new()

func _ready():
	load_options()
	print("Options Loaded")

func save_options() -> void:
	Save.write(options.serialize(), OPTIONS_PATH)

func load_options():
	var data = Save.read(OPTIONS_PATH)
	if data:
		options.deserialize(data)
	else:
		options = Options.new()
		save_options()
