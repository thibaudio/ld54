extends Control

@onready var master_volume_slider = $MarginContainer/VBoxContainer/VolumeSelector/Sliders/MasterVolumeSlider
@onready var effects_volume_slider = $MarginContainer/VBoxContainer/VolumeSelector/Sliders/EffectsVolumeSlider
@onready var music_volume_slider = $MarginContainer/VBoxContainer/VolumeSelector/Sliders/MusicVolumeSlider

func _ready():
	master_volume_slider.value = OptionsController.options.MasterVolume
	effects_volume_slider.value = OptionsController.options.EffectsVolume
	music_volume_slider.value = OptionsController.options.MusicVolume

func _on_Close_pressed():
	hide()

func _on_MasterVolumeSlider_drag_ended(value_changed):
	if value_changed:	
		Events.options_changed.emit()

func _on_EffectsVolumeSlider_drag_ended(value_changed):
	if value_changed:	
		Events.options_changed.emit()

func _on_MusicVolumeSlider_drag_ended(value_changed):
	if value_changed:	
		Events.options_changed.emit()
