extends CanvasLayer

@onready var color_rect = $ColorRect
@onready var continue_button = $ColorRect/VBoxContainer/ContinueButton
@onready var new_game_button = $ColorRect/VBoxContainer/NewGameButton
@onready var main_menu_button = $ColorRect/VBoxContainer/MainMenuButton

@export var show_in_ready = true
@export var in_game = true
var displayed

func _ready():
	if show_in_ready: 
		show_menu()
	else:
		hide_menu()

func _unhandled_input(event):
	if not in_game:
		return
		
	if event.is_action_pressed("ui_cancel"):
		toggle_menu()

func toggle_menu():
	if displayed:
		hide_menu()
	else:
		show_menu()

func hide_menu():
	displayed = false
	color_rect.hide()
	get_tree().paused = false

func show_menu():
	if in_game: 
		continue_button.show()
		continue_button.grab_focus()
		main_menu_button.show()
	else:
		continue_button.hide()
		new_game_button.grab_focus()
		main_menu_button.hide()
	displayed = true
	color_rect.show()
	get_tree().paused = true

func _on_continue_button_pressed():
	hide_menu()

func _on_new_game_button_pressed():
	hide_menu()
	GameManager.new_game()

func _on_settings_button_pressed():
	pass # Replace with function body.

func _on_quit_button_pressed():
	get_tree().quit()

func _on_main_menu_button_pressed():
	GameManager.main_menu()
