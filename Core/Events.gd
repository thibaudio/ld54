extends Node

signal options_changed

signal upgrade_selected(selected: bool)
signal over_upgrade(upgrade)
signal not_over_upgrade(upgrade)
signal game_state_changed()

signal not_enought(type: Globals.GameStateType)
