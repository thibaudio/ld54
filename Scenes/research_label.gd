extends Label

var original_text
func _ready():
	original_text = text
	
func _process(delta):
	text = original_text + " (%d" % GameManager.available_upgrades + "/%d)" % GameManager.current_game_state.researches_per_rounds
